Num = 10;
N = 30;
xx1d = linspace(-1,1,Num*10);
[xxpos,yypos] = meshgrid(xx1d);
out = (xxpos.^2+ yypos.^2) > 1;
xxpos(out) = [];
yypos(out) = [];
FS = 'fontsize'; MS = 'markersize';
xxpos = xxpos(:); yypos = yypos(:);
xx = xxpos; yy = yypos;
f = sin(xx.*yy);

q2 = ones(size(xx));
q2 = q2(:,1)/norm(q2(:,1));
count = 0;
for k = 2:N
    for j = 1:k
        count = count+1;
        if j == k
            q2 = [q2 q2(:,count+1-k).*yy];
        else
            q2 = [q2 q2(:,count+2-k).*xx];
        end
        for p = 1:count
           r(:,p) = q2(:,p)'*q2(:,count+1);
           q2(:,count+1) = q2(:,count+1)-r(p)*q2(:,p);
        end
        q2(:,count+1) = q2(:,count+1)/norm(q2(:,count+1));
    end
end

ind = randperm(length(xx),10)';
ind0 = ind;
for n = 6:300

A = q2(ind,1:n);
AA = q2(:,1:n);

fit = AA*(A\f(ind));

err = abs(fit-f);

clf, subplot(1,2,1)
plot(xx(ind), yy(ind),'.k',xx(ind0),yy(ind0),'o',MS,15); 

hold on

[maxerr, indrow] = max(err);
[maxerr2, indcol] = max(maxerr);
x1 = xx(indrow(indcol));
y1 = yy(indrow(indcol));
ind =[ind; indrow(indcol)];
p(n-5,1) = n;
p(n-5,2) = maxerr;
end
subplot(1,2,2)
semilogy(p(:,1), p(:,2),".-r");
hold off