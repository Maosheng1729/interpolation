theta = linspace(0,2*pi,100);
xc = (1+0.1*sin(5*theta)).*cos(theta)/1.1;
yc = (1+0.1*sin(5*theta)).*sin(theta)/1.1;
plot(xc,yc), axis image

[xx,yy]=meshgrid(linspace(-1,1,100));

ind = inpolygon(xx,yy,xc,yc);

xxin = xx(ind);
yyin = yy(ind);

xxout = xx(~ind);
yyout = yy(~ind);

hold on
plot(xxin,yyin,'*')
plot(xxout,yyout,'o')
hold off