%theta = linspace(0,2*pi,200)';
%Boundary points for Cassini
%{
e = 1.1;
XB = [];
YB = [];
for k = 1:length(theta)
    th = theta(k);
    f = chebfun(@(r) r.^4-9/8*r.^2.*cos(2*th) - 81/256*(e^4-1),[0,2]);
    rts = roots(f);
    for j = 1:length(rts)
        XB(end+1) = rts(j).*cos(th);
        YB(end+1) = rts(j).*sin(th);
    end
end
%}
%Boundary points for circles
%{
XB = max(max(R))*cos(theta);
YB = max(max(R))*sin(theta);
%}
%Boundary points for triangles
%{
XB = [-1,-1,1]';
YB = [-1,1,-1]';
%}
%Number of starting points
N = 100;
[~,~,E] = qr(q2(:,1:N)','vector');
x = xx(E(1:N));
y = yy(E(1:N));
while N < 1600
[x,y] = candidatepts(x,y,ceil(sqrt(4*N)),2*N,XB,YB);
N = 2*N;
disp(N)
end