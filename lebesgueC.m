function [lambdaM] = lebesgueC(A)
[row, ~] = size(A);
P = zeros(row,1);
%A(A>1)=1;
%A(A<-1)=-1;
a = max(1,max(abs(A(1,:))));
for i = 1:row
 [~, lambda] = lebesgue(A(i,:),-a,a);
 P(i)=lambda;
end
lambdaM = max(P);
end
