N2 = 100;
N3 = 40;
partition = 30;
r = zeros(N2,2);
t = zeros(partition-4,2);
for n = 1:N2
[Q,R,E] = qr(q2(:,1:n)','vector');
xpts = xx(E(1:n));
ypts = yy(E(1:n));
A2 = q2(E(1:n),1:n);
AA2 = q2(:,1:n);
fit2 = AA2*(A2\f(E(1:n)));
err2 = abs(f-fit2);
[qmaxerr, qindrow] = max(err2);
r(n,1) = n;
r(n,2) = qmaxerr;
end

clf, subplot(2,2,1)
plot(xpts,ypts,"*");
subplot(2,2,2)
semilogy(r(:,1),r(:,2),".-r")

for l = 5:partition
xxp = xpts; yyp = ypts;
nbins=[partition partition];
[N,C]=hist3([xpts, ypts],nbins);
for m = 1:l
    lowerx(m) = -1+2*(m-1)/l;
    upperx(m) = -1+2*m/l;
    for n = 1:l
    lowery(n) = -1+2*(n-1)/l;
    uppery(n) = -1+2*n/l;
    if(N(m,n) ~= 0)
        [xp,yp] = meshgrid(lowerx(m):1/(Num*(N(m,n))):upperx(m),lowery(n):1/(Num*(N(m,n))):uppery(n));
    end
    xp = xp(:); yp = yp(:); 
    xxp = [xxp; xp]; yyp = [yyp; yp];
    end
end
   
    out = (xxp.^2+ yyp.^2) > 1;
    xxp(out) = [];
    yyp(out) = [];
    f2 = xxp.*yyp;
    q3 = ones(size(xxp));
    q3 = q3(:,1)/norm(q3(:,1));
    count = 0;
    for k = 2:N3
        for j = 1:k
            count = count+1;     
            if j == k
                q3 = [q3 q3(:,count+1-k).*yyp];
            else
                q3 = [q3 q3(:,count+2-k).*xxp];
            end
            for p = 1:count
                r(:,p) = q3(:,p)'*q3(:,count+1);
                q3(:,count+1) = q3(:,count+1)-r(p)*q3(:,p);
            end
            q3(:,count+1) = q3(:,count+1)/norm(q3(:,count+1));
        end
    end
    [Q,R,E] = qr(q3(:,1:N2)','vector');
    A3 = q3(E(1:N2),1:N2);
    AA3 = q3(:,1:N2);
    fit3 = AA3*(A3\f2(E(1:N2)));
    err3 = abs(f2-fit3);
    [qmaxerr2, qindrow2] = max(err3);
    t(l-4,1) = l;
    t(l-4,2) = qmaxerr2;
end
 min = t(1,1);
 minVal = t(1,2);
for k = 2: partition-4
    if(minVal > t(k,2))
        minVal = t(k,2);
        min = t(k,1);
    end
end
xxp = xpts; yyp = ypts;
nbins=[min min];
[N,C]=hist3([xpts, ypts],nbins);
for m = 1:min
    lowerx(m) = -1+2*(m-1)/min;
    upperx(m) = -1+2*m/min;
    for n = 1:min
    lowery(n) = -1+2*(n-1)/min;
    uppery(n) = -1+2*n/min;
    if(N(m,n) ~= 0)
        [xp,yp] = meshgrid(lowerx(m):1/(Num*(N(m,n))):upperx(m),lowery(n):1/(Num*(N(m,n))):uppery(n));
    end
    xp = xp(:); yp = yp(:); 
    xxp = [xxp; xp]; yyp = [yyp; yp];
    end
end
out = (xxp.^2+ yyp.^2) > 1;
xxp(out) = [];
yyp(out) = [];
subplot(2,2,3)
plot(xxp,yyp,'.r');
subplot(2,2,4)
semilogy(t(:,1),t(:,2),".-b")
shg