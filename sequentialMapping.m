%% Finding good nodes by sequntial maps

%clear all
dP = 9;                    % degree for mapping (odd)
xx= linspace(-1,1,20)';    % equispaced points (initial points)

for j = 1:5
    
    x = xx(:,end);
    xx = linspace(-1,1,length(x)+10)';
    
    A = @(x) cos(acos(x)*(0:length(x)-1)); % interpolation matrix using Chebyshev pols
    
    C0 = zeros(length(1:2:dP),1); C0(1) = 1;  % initial guess for coefficients of mapping
    
    % loop for generating a sequence of mappings
    for k = 1:4
        
        P = @(c,x) cos(acos(x)*(1:2:dP))*c;  % mapping function
        
        disp(cond(A(x(:,k)))) % condition number of the interpolation matrix at the mapped points
        
        figure(1) % show interpolation points
        plot(x(:,k),k-1,'*'), hold on
        title(num2str(length(x)))
        shg
        
        [C,Fval] = fminsearch(@(c) cond(A(P(c,x(:,k)))),C0); % optimize the mapping function
        
        x(:,k+1) =  P(C,x(:,k));
        
        xmax = norm(x(:,k+1),inf);
        
        if xmax <=1, xmax = 1; end
        
        x(:,k+1) = x(:,k+1)/xmax;
        
        xx(:,k+1) = P(C,xx(:,k))/xmax;
        
    end
    
    % Chebyshev points for comparison
    xch = chebpts(length(x));
    plot(xch,k,'o')
    disp(cond(A(xch)))
    
    hold off
    
    pause
    
end