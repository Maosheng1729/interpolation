function [q3] = finalqr(x,y,degree,truncation)
[row,~] = size(x);
q3 = zeros(row,(degree+1)*degree/2);
q3(:,1) = ones(size(x));
q3(:,1) = q3(:,1)/norm(q3(:,1));
count = 0;
for k = 2:degree
    for j = 1:k
        count = count+1;     
        if j == k
            q3(:,count+1) = q3(:,count+1-k).*y;
        else
            q3(:,count+1) = q3(:,count+2-k).*x;
        end
    end     
    for p = 1:count
          r = q3(:,p)'*q3(:,count+1);
          q3(:,count+1) = q3(:,count+1)-r*q3(:,p);
    end
    q3(:,count+1) = q3(:,count+1)/norm(q3(:,count+1));
    if mod(count+1,10) == 0
       [Q,~] = qr(q3(:,[1:count+1]),0);
       q3(:,[1:count+1]) = Q(:,[1:count+1]);
    end
    if count+1 == truncation
       break
    end
end
[q3,~] = qr(q3,0);
end