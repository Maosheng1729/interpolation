%% Finding good nodes by sequential maps
hold off
%clear all
dP = 9;                    % degree for mapping (odd)
n = 9;
X = cell(10,1);
% equispaced points (initial points)
for k=1:numel(X)
    [x,y]= meshgrid(linspace(-1,1,n));
    xy = [x(:),y(:)];
    X(k) = {xy};
    n = n+2;
end

 %A = @(x) cos(acos(x)*(0:length(x)-1)); % interpolation matrix using Chebyshev pols
    
 C0 = zeros(length(1:2:dP),2); C0(1,1)=1;C0(1,2)=1; % initial guess for coefficients of mapping
    
 P = @(c,x) cos(acos(x)*(1:2:dP))*c;  % mapping function
 
for j = 1:1
   
    % loop for generating a sequence of mappings
    for k = 1:5
        
        x = X{j}(:,1);
        y = X{j}(:,2);
        %disp(X{j})
        disp(length(x)+": " +est2D(x',y')) % condition number of the interpolation matrix at the mapped points
        
        figure(1) % show interpolation points
        plot(x,y,'*'), hold on
        title(num2str(length(x)))
        shg
        
        %xmax = norm(real(x),inf);
        %if xmax <=1, xmax = 1; end
        
        %disp(xmax); 
        Penalty = @(x,y)1+100*tanh(10*(abs(x)-1)).*double(abs(x)>1)+100*tanh(10*(abs(y)-1)).*double(abs(y)>1);
        %Penalty = @(x)1.1.^((abs(2.1*x)-1).^40)+1000000*double(abs(x)>1);
        %Penalty = @(x) 1000000*double(abs(x)>1);
        %M = cos(acos(x)*(1:2:dP));
        [C Fval]= fminsearch(@(c) est2D(P(c(:,1),x),P(c(:,2),y))*max(Penalty(P(c(:,1),x)',P(c(:,2),y)')),C0); % optimize the mapping function
        %[C Fval] = fmincon(@(c) lebesgueC(P(c,x)').*max(Penalty(P(c,x)')),C0,M,O);
        
        for kk = j:numel(X)
            X{kk} =  [P(C(:,1),X{kk}(:,1)),P(C(:,2),X{kk}(:,2))];
        end
        
    end
    
    %Chebyshev points for comparison
    %[xch,ych] = meshgrid(chebpts(length(x)));
    %plot(xch,k,'o')
    %disp("Chebychev: "+est2D(xch,ych))

    hold off
    %pause
end