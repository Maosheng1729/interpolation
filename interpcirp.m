Num = 14;
[T,R] = meshgrid(linspace(0,2*pi,Num^2),linspace(0,1,16));
xxpos = R.*cos(T);
yypos = R.*sin(T);
N = 30;
FS = 'fontsize'; MS = 'markersize';
xxpos = xxpos(:); yypos = yypos(:);
xx = xxpos; yy = yypos;
f = 1./(1+16*xx.^2)+1./(1+16*yy.^2);
q2 = finalqr(xx,yy,N,100);
ind = randperm(length(xx),10)';
ind0 = ind;
for n = 6:300

A = q2(ind,1:n);
AA = q2(:,1:n);

fit = AA*(A\f(ind));

err = abs(fit-f);

plot(xx(ind), yy(ind),'.k',xx(ind0),yy(ind0),'o',MS,15); 

hold on

[maxerr, indrow] = max(err);
[maxerr2, indcol] = max(maxerr);
x1 = xx(indrow(indcol));
y1 = yy(indrow(indcol));
ind =[ind; indrow(indcol)];
end

hold off