%% Finding good nodes by sequential maps
hold off
%clear all
dP = 9;                    % degree for mapping (odd)
n = 15;
X = cell(5,1);
% equispaced points (initial points)
for k=1:20
    X(k) = {linspace(-1,1,n)'};
    n = n+5;
end
%{
xx = linspace(-1,1,25)';    % equispaced points (initial points)
xx2 = linspace(-1,1,numel(xx)+5)';
%}
xx = linspace(-1,1,5000)';

 A = @(x) cos(acos(x)*(0:length(x)-1)); % interpolation matrix using Chebyshev pols
    
 C0 = zeros(length(1:2:dP),1); C0(1) = 1;  % initial guess for coefficients of mapping
    
 P = @(c,x) cos(acos(x)*(1:2:dP))*c;  % mapping function
 
for j = 1:length(X)
    
    
   
    % loop for generating a sequence of mappings
    for k = 1:2
        
        x = X{j};
        disp(x);
        disp(lebesgueC(x')) % condition number of the interpolation matrix at the mapped points
        
        figure(1) % show interpolation points
        plot(x,k-1,'*'), hold on
        title(num2str(length(x)))
        shg
        
        %xmax = norm(real(x),inf);
        %if xmax <=1, xmax = 1; end
        
        %disp(xmax); 
        %Penalty = @(x)1.1.^((abs(2.1*x)-1).^40)+10*double(abs(x)>1);
        Penalty = @(x)1+100*tanh(10*(abs(x)-1)).*double(abs(x)>1);

        %Penalty = @(x) 1000000*double(abs(x)>1)+1;
        O = ones(length(x),1);
        M = cos(acos(x)*(1:2:dP));
        [C Fval]= fminsearch(@(c) lebEst(P(c,x),xx)*max(Penalty(P(c,x)')),C0); % optimize the mapping function
        %[C Fval] = fmincon(@(c) lebesgueC(P(c,x)').*max(Penalty(P(c,x)')),C0,M,O);
        
        
        %{
        x(:,k+1) =  P(C,x(:,k));
        
        xmax2 = norm(real(x(:,k+1)),inf);
    
        %if xmax2 <=1, xmax2 = 1; end
        
        x(:,k+1) = real(x(:,k+1))/xmax2;
        
        xx(:,k+1) = real(P(C,xx(:,k)));
        %}
        
        
        for kk = j:length(X)
            X{kk} =  real(P(C,X{kk}));
            %{
            Xmax = max(X{kk});
            X{kk} = X{kk}/Xmax;
            %}
        end
        
    end
    
    
    %Chebyshev points for comparison
    xch = chebpts(length(x));
    plot(xch,k,'o')
    %disp('mapped ')
    %disp(lebesgue(x))
    %disp('Chebpts ')
    %disp(lebesgue(xch))
  

    hold off
    %pause
end