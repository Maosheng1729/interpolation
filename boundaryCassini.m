% Boundary points for Cassini
e = 1.01;
theta = linspace(0,2*pi,200)';
xb = [];
yb = [];
for k = 1:length(theta)
    th = theta(k);
    f = chebfun(@(r) r.^4-9/8*r.^2.*cos(2*th) - 81/256*(e^4-1),[0,2]);
    rts = roots(f);
    for j = 1:length(rts)
        xb(end+1) = rts(j).*cos(th);
        yb(end+1) = rts(j).*sin(th);
    end
end

plot(xb,yb,'*');
axis image
shg