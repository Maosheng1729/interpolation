function [est,A] = est2D(z,n)
A = zeros(numel(z), (n+1)^2);
NumCol=1;
for j = 0:n
    for k = 0:n
        A(:,NumCol)=cos((k)*acos(real(z))).*cos((j)*acos(imag(z)));
        NumCol=NumCol+1;
    end
end
% L=sum(abs(A),2);
% est = max(L);
% F = find(L==est);
est = cond(A);
end
