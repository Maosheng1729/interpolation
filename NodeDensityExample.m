%% estimating node density functions 1D

Nbins = 11;
%xch = linspace(-1,1,300);%
xch = chebpts(300);

%xeq = linspace(-1,1,Nbins);
%density = zeros(length(xeq)-1,1);

%for k = 1:length(xeq)-1
    %ind = find(xch >= xeq(k) & xch < xeq(k+1));
    %density(k) = length(ind)/length(xch)/(xeq(2)-xeq(1));
%end

[N,xeq] = hist(xch,Nbins);
density = N/length(xch)/(xeq(2)-xeq(1));

%plot((xeq(1:end-1)+xeq(2:end))/2, density,'*')
plot(xeq, density,'*')


hold on
xx = linspace(-1,1,1000);

% Chebyshev node density
plot(xx,1./(pi*sqrt(1-xx.^2)));

% equispaced points
plot(xx,1/2+0*xx,'--')

ylim([0 2])
hold off



