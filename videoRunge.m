h = figure;
v = VideoWriter('runge.avi');
v.FrameRate = 4;
open(v);
h.Visible = 'off';
x = linspace(-1,1,500);
f = @(x)(1./(1+25*x.^2));
for n = 8:4:70
    xx = linspace(0,pi,n);
    zz = cos(xx);
    %p = polyfit(zz,f(zz),n-1);
    %yy = polyval(p,x);
    w = baryWeights(zz.');
    yy = bary(x.',f(zz.'),zz.',w);
    clf
    plot(x,f(x),'b'); hold on, plot(zz,f(zz),'.k','markersize',15); plot(x,yy,'r'); hold off
    axis square
    frame = getframe(h);
    writeVideo(v,frame);
end
h.Visible = 'on';
close(v);