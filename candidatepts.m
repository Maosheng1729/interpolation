function [xfin,yfin] = candidatepts(xx,yy,degree,result,XB,YB)
%Plot the original candidate points
clf, subplot(2,2,1)
plot(xx,yy,"*");
axis image
min = 11;
[xxp, yyp] = partition(xx,yy,min,XB,YB);
ind = inpolygon(xxp,yyp,XB,YB);
xxin = xxp(ind);
yyin = yyp(ind);
%Plot all of the points in the domain that
%are populated through partition in addition
%to the original candiate points
subplot(2,2,2)
plot(xxin,yyin,'.r');
axis image
q3 = finalqr(xxin,yyin,degree,100);
[~,~,E] = qr(q3(:,1:result)','vector');
xfin = xxin(E(1:result));
yfin = yyin(E(1:result));
%Plot the new candidate points
subplot(2,2,3)
plot(xfin,yfin,'*');
axis image
end