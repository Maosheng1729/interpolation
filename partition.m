function [xxp,yyp] = partition(x,y,bin,XB,YB)
xxp = x; yyp = y;
density = 3;
%Forming a grid of [-gridX,gridX] X [-gridY,gridY]
gridX = max(abs(XB));
gridY = max(abs(YB));
%Start partitioning the grid into bin X bin parts
nbins=[bin bin];
[N,~]=hist3([xxp, yyp],nbins);
for m = 1:bin
    lowerx(m) = -gridX+2*gridX*(m-1)/bin;
    upperx(m) = -gridX+2*gridX*m/bin;
    for n = 1:bin
    lowery(n) = -gridY+2*gridY*(n-1)/bin;
    uppery(n) = -gridY+2*gridY*n/bin;
    if(N(m,n) ~= 0)
        [xp,yp] = meshgrid(lowerx(m):(upperx(m)-lowerx(m))/(density*(N(m,n))):upperx(m),...
        lowery(n):(uppery(n)-lowery(n))/(density*(N(m,n))):uppery(n));
        ind1 = inpolygon(xp,yp,XB,YB);
        ratio = sqrt(nnz(ind1)/numel(xp));
        if ratio < 1
            [xp,yp] = meshgrid(lowerx(m):ratio*(upperx(m)-lowerx(m))/(density*(N(m,n))):upperx(m),...
            lowery(n):ratio*(uppery(n)-lowery(n))/(density*(N(m,n))):uppery(n));
        end
    else 
        [xp,yp] = meshgrid(lowerx(m):(upperx(m)-lowerx(m))/(density-1):upperx(m),...
        lowery(n):(uppery(n)-lowery(n))/(density-1):uppery(n));
    end
    xp = xp(:); yp = yp(:); 
    xxp = [xxp; xp]; yyp = [yyp; yp];
    end
end
end