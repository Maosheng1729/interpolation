%% Finding good nodes by sequntial maps

%clear all
dP = 9;                    % degree for mapping (odd)

n = 25;
X = cell(5,1);
% equispaced points (initial points)
for k=1:5
    X(k) = {linspace(-1,1,n)'};
    n = n+10;
end

L = legpoly(0:length(x)-1, [-1 1],'norm');

A = @(x) L(x,:); % interpolation matrix using Legendre pols

Lmap = legpoly((1:2:dP), [-1 1],'norm');
C0 = zeros(length((1:2:dP)),1); C0(1) = 1/Lmap(1,1);  % initial guess for coefficients of mapping

P = @(c,x) Lmap(x,:)*c;  % mapping function

% loop for generating a sequence of mappings

for j = 1:5
    
    for k = 1:5
        
         x = X{j};
        
        disp(cond(A(x))) % condition number of the interpolation matrix at the mapped points
        
        figure(1) % show interpolation points
        plot(x,k-1,'*'), hold on
        title(num2str(length(x)))
        shg
        
        [C,Fval] = fminsearch(@(c) cond(A(P(c,x))),C0) % optimize the mapping function
        
        for kk = j:5
            X{kk} =  P(C,X{kk});
        end
        
        pause
        
    end
    
    % Chebyshev points for comparison
    xch = legpts(length(x));
    plot(xch,k,'o')
    disp(cond(A(xch)))
    hold off
  
    
 end
    