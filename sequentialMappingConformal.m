%% Finding good nodes by sequential maps
hold off
%clear all
dP = 9;                    % degree for mapping (odd)
n =5;
X = cell(10,1);
% equispaced points (initial points)
for k=1:numel(X)
    [x,y]= meshgrid(linspace(-1,1,n));
    X(k) = {x(:)+1i*y(:)};
    n = n+2;
end

 %A = @(x) cos(acos(x)*(0:length(x)-1)); % interpolation matrix using Chebyshev pols
    
 C0 = zeros(length(0:dP),1); C0(2)=1; % initial guess for coefficients of mapping
    
 %P = @(c,x) cos(acos(x)*(1:dP))*c;  % mapping function
 P = @(c,x) (repmat(x,1,dP+1).^repmat(0:dP,length(x),1))*c;
 %P = @(c,x) [ones(size(x))
 
for j = 1
   
    % loop for generating a sequence of mappings
    for k = 1:10
        
        x = real(X{j});
        y = imag(X{j});
        %disp(X{j})
        disp(length(x)+": " +est2D(x+1i*y,n+2*j-3)); % condition number of the interpolation matrix at the mapped points
        figure(1) % show interpolation points
        plot(x,y,'*'), hold on
        title(num2str(length(x)))
        shg
        
        %xmax = norm(real(x),inf);
        %if xmax <=1, xmax = 1; end
        
        %disp(xmax); 
        Penalty = @(z)1+100*tanh(10*(abs(real(z))-1)).*double(abs(real(z))>1)+100*tanh(10*(abs(imag(z))-1)).*double(abs(imag(z))>1);
        %Penalty = @(z)1+100*tanh(10*(z*conj(z)-1));
        %Penalty = @(z)1.1.^((abs(2.1*x)-1).^40)+1000000*double(abs(x)>1);
        %Penalty = @(x) 1000000*double(abs(x)>1);
        %M = cos(acos(x)*(1:2:dP));
        [C, Fval]= fminsearch(@(c) est2D(P(c,x+1i*y),n+2*j-3)*max(Penalty(P(c,x+1i*y)')),C0); % optimize the mapping function
        %[C Fval] = fmincon(@(c) lebesgueC(P(c,x)').*max(Penalty(P(c,x)')),C0,M,O);
        %C0=C;
        for kk = j:numel(X)
            X{kk} =  P(C,X{kk});
        end
       hold off
       
    end
    
    %Chebyshev points for comparison
    %[xch,ych] = meshgrid(chebpts(length(x)));
    %plot(xch,k,'o')
    %disp("Chebychev: "+est2D(xch,ych))

    %pause
end