function Lm = lebEst(x,xx)

w = baryWeights(x); 
% Based on barycentric formula.
L = ones(size(xx)); % Note: L(x) = 1
mem = ismember(xx, x);
for i = 1:numel(xx)
    if ( ~mem(i) )
        xxx = w./(xx(i) - x);
        L(i) = sum(abs(xxx))/abs(sum(xxx));
    end
end
Lm = max(L);