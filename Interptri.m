%function C = greedy2(N)
Num = 7;
N = 30;
FS = 'fontsize'; MS = 'markersize';

xx1d = linspace(-1,1,Num*10);
[xxpos,yypos] = meshgrid(xx1d);
out = (xxpos+ yypos) > 0;
xxpos(out) = [];
yypos(out) = [];
xxpos = xxpos(:); yypos = yypos(:);
xx = xxpos; yy = yypos;
f = 1./(1+16*xx.^2)+1./(1+16*yy.^2);
q2 = finalqr(xx,yy,N,100);
ind = randperm(length(xx),10)';
ind0 = ind;
p = zeros(95,2);
for n = 6:100


A = q2(ind,1:n);
AA = q2(:,1:n);

fit = AA*(A\f(ind));


err = abs(fit-f);
clf, subplot(1,2,1)
plot(xx(ind), yy(ind),'.k',xx(ind0),yy(ind0),'o',MS,15);

hold on

[maxerr, indrow] = max(err);
[maxerr2, indcol] = max(maxerr);
x1 = xx(indrow(indcol));
y1 = yy(indrow(indcol));
ind =[ind; indrow(indcol)];
p(n-5,1) = n;
p(n-5,2) = maxerr;
end
subplot(1,2,2)
semilogy(p(:,1), p(:,2),".-r");
hold off