x = linspace(-1,1,20)';
y = sin(pi*x/2);
plot(x,0,'*')
hold on
plot(0,y,'*')
plot(x,y)
hold off